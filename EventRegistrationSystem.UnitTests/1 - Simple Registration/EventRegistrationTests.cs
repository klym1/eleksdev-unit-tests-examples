﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using EventRegistrationSystem.SimpleRegistration;

namespace EventRegistrationSystem.SimpleRegistration
{
    [TestFixture]
    class EventRegistrationTests
    {
        [Test]
        public void RegistrationStart_Should_Change_EventState_To_Registration()
        {
            //Arrange
            var @event = new Event();

            //Act
            @event.Start();

            //Assert
            Assert.That(@event.EventState == EventState.Registration);
        }

        [Test]
        public void Registration_Should_Fail_When_Event_State_Is_Not_Registration()
        {
            //Arrange
            var @event = new Event();
            var participant = new Participant(6)
            {
                FirstName = "Bruce",
                LastName = "Willis"
            };

            //Danger code
            Thread.Sleep(new Random().Next(4000));

            //Act/Assert
            Assert.That(() => @event.Register(participant),
                Throws.TypeOf<Exception>().With.Message.EqualTo("Can't register at the moment"));   
        }
        
        [Test]
        public void Registration_should_succedd_when_Event_state_is_Registration()
        {
            var @event = new Event();
            @event.Start();

            Assert.DoesNotThrow(() => @event.Register(new Participant(4567)));
        }
    }
}
