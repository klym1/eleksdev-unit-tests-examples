﻿using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace EventRegistrationSystem.With_Stub
{
    [TestFixture]
    class EventRegistrationTests
    {
        [Test]
        public void RegistrationStart_Should_Change_EventState_To_Registration()
        {
            var @event = new Event(new DummyLogger());
            @event.Start();
            
            @event.Register(new Participant(444));

            Assert.That(@event.Participants.Count == 1);
        }
    }
}
