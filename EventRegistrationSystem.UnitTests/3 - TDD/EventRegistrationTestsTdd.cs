﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace EventRegistrationSystem.With_Stub
{ 
    [TestFixture]
    class EventRegistrationTestsTdd
    {
        [Test]
        public void UnRegister_should_work()
        {
            var @event1 = new Event(new DummyLogger());
            @event1.Start();

            var participant = new Participant(89)
            {
                FirstName = "John",
                LastName = "Doe"
            };

            VerifyRegister(event1, participant);
            VerifyUnRegister(event1, participant);
        }

        private static void VerifyUnRegister(Event event1, Participant participant)
        {
            @event1.UnRegister(participant.Id);
            Assert.That(@event1.Participants.All(it => it.Id != participant.Id));
        }

        private static void VerifyRegister(Event event1, Participant participant)
        {
            @event1.Register(participant);
            Assert.That(@event1.Participants.Any(it => it.Id == participant.Id));
        }
    }
}
