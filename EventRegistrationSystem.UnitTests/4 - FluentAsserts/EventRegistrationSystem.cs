﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;

namespace EventRegistrationSystem.With_Stub
{
    [TestFixture]
    class EventRegistrationTests_Fluent
    {
        [Test]
        public void RegistrationStart_Should_Change_EventState_To_Registration()
        {
            var @event = new Event(new DummyLogger());
            @event.Start();

            @event.Register(new Participant(12));

            @event.Participants
                .Should()
                .HaveCount(1);
        }
    }
}
