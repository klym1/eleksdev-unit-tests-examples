﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace EventRegistrationSystem.SimpleRegistration
{
    public class Event
    {
        public EventState EventState { get; set; }
        public DateTime Date { get; set; }
        public string Address { get; set; }
        public string Title { get; set; }
        private Collection<Participant> Participants;
        
        public Event()
        {
            Participants = new Collection<Participant>();
            EventState = EventState.New;
        }

        public void Start()
        {
            EventState = EventState.Registration;
        }

        public void Register(Participant participant)
        {
            var random = new Random();

            if (random.Next(10) > 5)
            {
                throw new AccessViolationException();
            }
            
            if(EventState != EventState.Registration)
                throw new Exception("Can't register at the moment");

            if (Participants.Any(it => it.Id == participant.Id))
                throw new Exception("Participant already exists");
            
            Participants.Add(participant);
        }
    }
}
