﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EventRegistrationSystem.With_Stub
{
    public class Event
    {
        public EventState EventState { get; set; }
        public DateTime Date { get; set; }
        public string Address { get; set; }
        public string Title { get; set; }
        public Collection<Participant> Participants { get; set; }

        private ILogger _logger;

        public Event(ILogger logger)
        {
            _logger = logger;
            Participants = new Collection<Participant>();
        }

        public void Start()
        {
            EventState = EventState.Registration;
        }

        public void Register(Participant participant)
        {
            if (EventState != EventState.Registration)
                throw new Exception("Can't register at the moment");

            if (Participants.Any(it => it.Id == participant.Id))
                throw new Exception("Participant already exists");

            Participants.Add(participant);

            _logger.Log("Participant added");
        }

        public void UnRegister(int id)
        {
            var participant = Participants.First(it => it.Id == id);
            Participants.Remove(participant);
        }
    }

    public interface ILogger
    {
        void Log(string message);
    }

    public class ConsoleLogger : ILogger
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }

    public class DummyLogger : ILogger
    {
        public void Log(string message)
        {
        }
    }

    public class DbLogger : ILogger
    {
        public void Log(string message)
        {
            Thread.Sleep(5000);
        }
    }
}
