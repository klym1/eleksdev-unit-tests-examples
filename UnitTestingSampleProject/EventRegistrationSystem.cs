﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventRegistrationSystem
{
    public enum EventState
    {
        New,
        Registration,
        OnAir,
        Ended
    }

    public class Event
    {
        public EventState EventState { get; set; }
        public DateTime Date { get; set; }
        public string Address { get; set; }
        public string Title { get; set; }
        public Collection<Participant> Participants { get; set; }
    }

    public class Participant
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Id { get; }

        public Participant(int id)
        {
            Id = id;
        }
    }
}
